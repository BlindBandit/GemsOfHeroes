import { _decorator, Component, Node, instantiate, Prefab, Vec2, Vec3 } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Field')
export class Field extends Component {
    @property(Prefab)
    tilePrefab: Prefab = null;

    @property
    numRows: number = 8;

    @property
    numCols: number = 8;

    @property
    tileSpacing: number = 5;
    @property
    xOffset: number = -150;
    @property
    yOffset: number = -175;

    private tileArray: Node[][] = [];

    private isSwapAvailable: bool = false;


    start() {
        this.spawnInitialBoard();
    }

    /*init field*/
    spawnRandomTile(row: number, col: number): Node {
        const tileType = Math.floor(Math.random() * 5);
        const tileNode = instantiate(this.tilePrefab);
        const tileComponent = tileNode.getComponent("Tile");

        tileComponent.init(tileType, row, col);
        const posX = col * (tileNode.width + this.tileSpacing) + this.xOffset;
        const posY = row * (tileNode.height + this.tileSpacing) + this.yOffset;
        tileNode.setPosition(posX, posY + tileNode.height);

        cc.tween(tileNode)
            .to(0.2, { position: new Vec3(posX, posY, 0) })
            .start();

        tileNode.on("swap", (col, row, direction) => {
            this.trySwapTiles(col, row, direction);
        });

        return tileNode;
    }


    spawnInitialBoard() {
        for (let row = 0; row < this.numRows; row++) {
            this.tileArray[row] = [];
            for (let col = 0; col < this.numCols; col++) {
                let tile = this.spawnRandomTile(row, col);
    
                while (this.checkVerticalMatches(tile)) {
                    tile.destroy();
                    const newTile = this.spawnRandomTile(row, col);
                    tile = newTile;
                }
    
                while (this.checkHorizontalMatches(tile)) {
                    tile.destroy();
                    const newTile = this.spawnRandomTile(row, col);
                    tile = newTile;
                }
    
                this.tileArray[row][col] = tile;
                this.node.addChild(tile);
            }
        }

        this.isSwapAvailable = true;
    }
    
    checkVerticalMatches(tile: Node): boolean {
        const row = tile.getComponent("Tile").getRow();
        const col = tile.getComponent("Tile").getCol();
        const tileType = tile.getComponent("Tile").getTileType();
    
        if (row >= 2) {
            if (this.tileArray[row - 1][col].getComponent("Tile").getTileType() === tileType &&
                this.tileArray[row - 2][col].getComponent("Tile").getTileType() === tileType) {
                return true;
            }
        }
    
        return false;
    }
    
    checkHorizontalMatches(tile: Node): boolean {
        const row = tile.getComponent("Tile").getRow();
        const col = tile.getComponent("Tile").getCol();
        const tileType = tile.getComponent("Tile").getTileType();
    
        if (col >= 2) {
            if (this.tileArray[row][col - 1].getComponent("Tile").getTileType() === tileType &&
                this.tileArray[row][col - 2].getComponent("Tile").getTileType() === tileType) {
                return true;
            }
        }
    
        return false;
    }

    /*swap logic*/
    trySwapTiles(row: number, col: number, direction: string) {
        if(!this.isSwapAvailable) {
            return;
        }

        let selectedTile: cc.Node = this.tileArray[row][col];
        let targetTile: cc.Node = null;
    
        switch (direction) {
            case "up":
                if(row >= this.numRows - 1) {
                    return;
                }
                targetTile = this.tileArray[row + 1][col];
                break;
            case "down":
                if(row <= 0) {
                    return;
                }
                targetTile = this.tileArray[row - 1][col];
                break;
            case "left":
                if(col <= 0) {
                    return;
                }
                targetTile = this.tileArray[row][col - 1];
                break;
            case "right":
                if(col >= this.numCols - 1) {
                    return;
                }
                targetTile = this.tileArray[row][col + 1];
                break;
            default:
                break;
        }
    
        this.swapTiles(selectedTile, targetTile, false);
    }

    swapTiles(tile1: cc.Node, tile2: cc.Node, isBackward: boolean) {
        this.isSwapAvailable = false;

        const pos1 = tile1.getPosition();
        const pos2 = tile2.getPosition();

        let tileComponent1 = tile1.getComponent("Tile");
        let tileComponent2 = tile2.getComponent("Tile");

        const col1 = tileComponent1.getCol();
        const col2 = tileComponent2.getCol();
        const row1 = tileComponent1.getRow();
        const row2 = tileComponent2.getRow();
    
        cc.tween(tile1)
            .to(0.3, { position: pos2 })
            .call(() => { 
                tileComponent1.setCol(col2);
                tileComponent1.setRow(row2);
                this.tileArray[row2][col2] = tile1;
             })
            .start();
    
        cc.tween(tile2)
            .to(0.3, { position: pos1 })
            .call(() => { 
                tileComponent2.setCol(col1);
                tileComponent2.setRow(row1);
                this.tileArray[row1][col1] = tile2;
                if(isBackward) {
                    this.isSwapAvailable = true;
                    return;
                }
                else {
                    let isMatchesFound = this.findAndDestroyMatches();
                    console.log(isMatchesFound);
                    if(!isMatchesFound) {
                        this.swapTiles(tile2, tile1, true);
                    }
                    else {
                        this.spawnNewTiles();
                    }
                }
             })
            .start();
    }


    /*matches logic*/
    findAndDestroyMatches(): boolean {
        let isMatchesFound = false;
        let tilesToNull = [];

        for (let row = 0; row < this.numRows; row++) {
            for (let col = 0; col < this.numCols; col++) {
                const tile = this.tileArray[row][col];
                const matches = this.findMatches(tile);
                
                if (matches.length >= 3) {
                    matches.forEach(matchedTile => {
                        let tileComponent = matchedTile.getComponent("Tile");
                        tilesToNull.push(new Vec2(tileComponent.getRow(), tileComponent.getCol()));
                        matchedTile.destroy();
                    });

                    isMatchesFound = true;
                }
            }
        }

        for(let i = 0; i < tilesToNull.length; i++) {
            this.tileArray[tilesToNull[i].x][tilesToNull[i].y] = null;
        }

        return isMatchesFound;
    }

    findMatches(tile: Node): Node[] {
        let matches: Node[] = [];
        const tileComponent = tile.getComponent("Tile");
        matches.push(tile);

        const horizontalMatches = this.checkMatchesInDirection(tileComponent, 1, 0)
            .concat(this.checkMatchesInDirection(tileComponent, -1, 0));

        if (horizontalMatches.length >= 2) {
            matches = matches.concat(horizontalMatches);
        }

        const verticalMatches = this.checkMatchesInDirection(tileComponent, 0, 1)
            .concat(this.checkMatchesInDirection(tileComponent, 0, -1));

        if (verticalMatches.length >= 2) {
            matches = matches.concat(verticalMatches);
        }

        return matches;
    }

    checkMatchesInDirection(tileComponent: any, dirX: number, dirY: number): Node[] {
        const row = tileComponent.getRow();
        const col = tileComponent.getCol();
        const tileType = tileComponent.getTileType();
        const matches: Node[] = [];

        let currentRow = row + dirY;
        let currentCol = col + dirX;

        while (currentRow >= 0 && currentRow < this.numRows &&
            currentCol >= 0 && currentCol < this.numCols) {
            const currentTile = this.tileArray[currentRow][currentCol];
            const currentTileComponent = currentTile.getComponent("Tile");

            if (currentTileComponent.getTileType() === tileType) {
                matches.push(currentTile);
                currentRow += dirY;
                currentCol += dirX;
            } else {
                break;
            }
        }

        return matches;
    }

    spawnNewTiles() {
        for (let col = 0; col < this.numCols; col++) {

            let emptySpaces = 0;

            for (let row = 0; row < this.numRows; row++) {
                const tile = this.tileArray[row][col];

                if (tile === null) {
                    emptySpaces++;
                }

                else {
                    if(emptySpaces > 0) {
                        let tileComponent = tile.getComponent("Tile");
                        tileComponent.setRow(row - emptySpaces);
                        this.tileArray[row - emptySpaces][col] = tile;
                        this.tileArray[row][col] = null;

                        const posX = col * (tile.width + this.tileSpacing) + this.xOffset;
                        const posY = (row - emptySpaces) * (tile.height + this.tileSpacing) + this.yOffset;
                        cc.tween(tile)
                            .to(0.2, { position: new Vec3(posX, posY, 0) })
                            .start();
                    }
                }
            }
        }

        this.scheduleOnce(() => {
            for (let col = 0; col < this.numCols; col++) {
                for (let row = 0; row < this.numRows; row++) {
                    const tile = this.tileArray[row][col];
                    if (tile === null) {
                        const newTile = this.spawnRandomTile(row, col);
                        this.tileArray[row][col] = newTile;
                        this.node.addChild(newTile);
                    }
                }
            }
        }, 0.25);

        this.scheduleOnce(() => {
            this.matchChain();
        }, 0.5);
    }


    matchChain() {
        let isMatchesFound = this.findAndDestroyMatches();
        if(!isMatchesFound) {
            this.isSwapAvailable = true;
        }
        else {
            this.spawnNewTiles();
        }
    }
}


