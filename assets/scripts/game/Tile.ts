import { _decorator, Component, Node, Sprite, SpriteFrame, Vec2, Vec3, UITransform } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Tile')
export class Tile extends Component {

    @property(Sprite)
    icon: Sprite = null;

    @property(SpriteFrame)
    blue: SpriteFrame | null = null;
    @property(SpriteFrame)
    red: SpriteFrame | null = null;
    @property(SpriteFrame)
    green: SpriteFrame | null = null;
    @property(SpriteFrame)
    yellow: SpriteFrame | null = null;
    @property(SpriteFrame)
    purple: SpriteFrame | null = null;

    private tileType: number;
    private row: number;
    private col: number;

    private startDragPos: Vec2 = cc.Vec2.ZERO;
    private isWaitingForNewTouch: boolean = false;


    onLoad() {
        this.node.on(Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
    }

    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
    }
    
    
    init(tileType: number, row: number, col: number) {
        this.tileType = tileType;
        this.row = row;
        this.col = col;
        
        switch(this.tileType) {
            case 0:
                this.icon.spriteFrame = this.blue;
                break;
            case 1:
                this.icon.spriteFrame = this.red;
                break;
            case 2:
                this.icon.spriteFrame = this.green;
                break;
            case 3:
                this.icon.spriteFrame = this.yellow;
                break;
            case 4:
                this.icon.spriteFrame = this.purple;
                break;
        }
    }

    getTileType(): number {
        return this.tileType;
    }

    getRow(): number {
        return this.row;
    }

    getCol(): number {
        return this.col;
    }

    setRow(_row: number) {
        this.row = _row;
    }

    setCol(_col: number) {
        this.col = _col;
    }


    onTouchStart(event: cc.Event.EventTouch) {
        const uiTransform = this.node.getComponent(UITransform);
        this.startDragPos = uiTransform.convertToNodeSpaceAR(new Vec3(event.getLocationX(), event.getLocationY(), 0));

        this.isWaitingForNewTouch = false;
    }
    
    onTouchMove(event: cc.Event.EventTouch) {
        if(this.isWaitingForNewTouch) {
            return;
        }

        const uiTransform = this.node.getComponent(UITransform);
        const touchPos = uiTransform.convertToNodeSpaceAR(new Vec3(event.getLocationX(), event.getLocationY(), 0));

        const deltaX = touchPos.x - this.startDragPos.x;
        const deltaY = touchPos.y - this.startDragPos.y;

        if (Math.abs(deltaX) > Math.abs(deltaY)) {
            if (deltaX > this.node.width) {
                this.isWaitingForNewTouch = true;
                this.node.emit("swap", this.row, this.col, "right");
            } else if (deltaX < -this.node.width) {
                this.isWaitingForNewTouch = true;
                this.node.emit("swap", this.row, this.col, "left");
            }
        } else {
            if (deltaY > this.node.height) {
                this.isWaitingForNewTouch = true;
                this.node.emit("swap", this.row, this.col, "up");
            } else if (deltaY < -this.node.height) {
                this.isWaitingForNewTouch = true;
                this.node.emit("swap", this.row, this.col, "down");
            }
        }
    }
}


